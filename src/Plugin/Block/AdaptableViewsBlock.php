<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Serialization\Json;
use Drupal\pagedesigner\Entity\Element;
use Drupal\views\Plugin\Block\ViewsBlock;

/**
 * Provides an adaptable views block.
 *
 * @Block(
 *   id = "adaptable_views_block",
 *   admin_label = @Translation("Adaptable Views Block"),
 *   category = @Translation("Adaptable Lists (Views)"),
 *   deriver = "Drupal\pagedesigner_block_adaptable\Plugin\Derivative\AdaptableViewsBlock"
 * )
 */
class AdaptableViewsBlock extends ViewsBlock implements AdaptableViewsBlockInterface {

  /**
   * The pagedesigner element to get the data from.
   *
   * @var \Drupal\pagedesigner\Entity\Element
   */
  protected $pagedesignerElement = NULL;

  /**
   * {@inheritdoc}
   */
  public function setPagedesignerElement(Element $pagedesignerElement) {
    $this->pagedesignerElement = $pagedesignerElement;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($this->pagedesignerElement != NULL) {
      // Store the original options.
      $filters = $this->view->getDisplay()->getOption('filters');
      $pager = $this->view->getDisplay()->getOption('pager');

      // Get the settings.
      $settings = Json::decode($this->pagedesignerElement->field_block_settings->value);

      // Alter the options for the build.
      if (!empty($settings['filters'])) {
        foreach ($settings['filters'] as $key => $item) {
          if (isset($item['type']) && 'string' == $item['type']) {
            if (is_array($item['value'])) {
              $settings['filters'][$key]['value'] = $item['value'][0];
            }
            else {
              $settings['filters'][$key]['value'] = $item['value'];
            }
          }
        }
        $this->alterFilters($settings['filters']);
      }
      if (!empty($settings['pager'])) {
        $this->alterPager($settings['pager']);
      }

      try {
        // Build the view.
        $build = parent::build();
      }
      catch (\Throwable $e) {
        // Something failed. Catch and log the error.
        \Drupal::logger('pagedesigner_block_adaptable')->error(
          new FormattableMarkup(
            'Error on building adaptable block. Element id: @id; Error: @error<br /><br />Stacktrace: @trace',
            [
              '@id' => $this->pagedesignerElement->id(),
              '@error' => $e->getMessage(),
              '@trace' => $e->getTraceAsString(),
            ]
          )
        );
        // Return non-cached response.
        $build = [
          '#cache' => ['max-age' => 0],
        ];
        // Inform user if logged in.
        if (\Drupal::currentUser()->isAuthenticated()) {
          $build['#markup'] = '<pre>' . $this->t('Something is misconfigured with the block. Check the settings.') . '</pre>';
        }
        return $build;
      }

      // Set the exposed input.
      $this->setExposedInput($build, $settings['filters'], $settings['pager']);

      // Hide the exposed filters and submit button.
      if (is_array($this->view->exposed_widgets)) {
        $this->view->exposed_widgets = array_diff_key($this->view->exposed_widgets, $filters);
        $this->view->exposed_widgets['actions']['submit']['#attributes']['style'][] = 'display:none;';
        $this->view->exposed_widgets['actions']['reset']['#attributes']['style'][] = 'display:none;';
      }

      // Reset the options for the next build.
      $this->view->getDisplay()->overrideOption('filters', $filters);
      $this->view->getDisplay()->overrideOption('pager', $pager);
    }
    else {
      $build = parent::build();
    }
    return $build;
  }

  /**
   * Alter the filter definition before rendering the block.
   *
   * @param array $customFilters
   *   The custom filters.
   */
  protected function alterFilters(array $customFilters) {
    $filters = $this->view->getDisplay()->getOption('filters');
    $filterManager = \Drupal::service('plugin.manager.pagedesigner_block_adaptable_filter');

    foreach ($customFilters as $key => $filter) {
      if ($key == 'content_type') {
        $key = 'type';
      }
      if (isset($filters[$key])) {
        if (!$filters[$key]['exposed'] && $filter['type'] != 'pba_entity_filter') {
          continue;
        }
        $filters[$key]['exposed'] = FALSE;
        $filterPlugin = $filterManager->getInstance(['type' => $filters[$key]['plugin_id']])[0];
        if ($filters[$key]['plugin_id'] == 'numeric') {
          $filters[$key]['value']['value'] = $filterPlugin->view($filters[$key], $filter['value']);
        }
        else {
          $filters[$key]['value'] = $filterPlugin->view($filters[$key], $filter['value']);
        }
      }
      elseif ($key == 'content_type') {
        $filters[$key]['exposed'] = FALSE;
        foreach ($filter['value'] as $filter_key => $item) {
          if ($item) {
            $filters[$key]['value'][$filter_key] = TRUE;
          }
          else {
            $filters[$key]['value'][$filter_key] = FALSE;
            $filters[$key]['value']['all'] = FALSE;
          }
        }
      }
    }
    $this->view->getDisplay()->overrideOption('filters', $filters);
  }

  /**
   * Alter the pager definition before rendering the block.
   *
   * @param array $customPager
   *   The custom pager.
   */
  protected function alterPager(array $customPager) {
    $pager = $this->view->getDisplay()->getOption('pager');
    foreach ($customPager as $key => $setting) {
      if (!empty($setting)) {
        $pager['options'][$key] = $setting;
      }
    }
    $this->view->getDisplay()->overrideOption('pager', $pager);
  }

  /**
   * Add the filter settings to the exposed input.
   *
   * @param array $build
   *   The build array with the view executable.
   * @param array $filters
   *   The filter settings as key => settings.
   * @param array $pager
   *   The pager settings as key => value.
   */
  protected function setExposedInput(array &$build, array $filters, array $pager = NULL) {
    $exposedInput = [];
    foreach ($filters as $key => $filter) {
      $exposedInput[$key] = [];
      if (is_array($filter['value'])) {
        $exposedInput[$key] = array_values($filter['value']);
      }
      elseif (is_scalar($filter['value'])) {
        $exposedInput[$key] = $filter['value'];
      }
    }
    // Add pager information if overwritten.
    if (!empty($pager)) {
      foreach ($pager as $key => $value) {
        $exposedInput[$key] = $value;
      }
    }
    $build['#view']->setExposedInput(
      array_merge(
        $build['#view']->getExposedInput(),
        $exposedInput
      )
    );
  }

}
