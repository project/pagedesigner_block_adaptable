<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\Block;

use Drupal\pagedesigner\Entity\Element;

/**
 * Provides the interface for an adaptable views block.
 */
interface AdaptableViewsBlockInterface {

  /**
   * Set the pagedesigner element to get the data from.
   *
   * @param \Drupal\pagedesigner\Entity\Element $pagedesignerElement
   *   The pagedesigner element to get the data from.
   */
  public function setPagedesignerElement(Element $pagedesignerElement);

}
