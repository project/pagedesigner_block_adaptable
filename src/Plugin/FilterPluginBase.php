<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\FilterPluginInterface;

/**
 * Base implementation for pagedesigner block filters.
 */
abstract class FilterPluginBase extends PluginBase implements FilterPluginInterface {

  /**
   * {@inheritDoc}
   */
  public function serialize(array &$filterDefinition = NULL, $value = NULL) : array {
    if (!is_array($value)) {
      $value = [$value];
    }
    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$filterDefinition, $value) {
    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function patchPrepare(array &$filterDefinition, $value) {
    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function adjustFilter(array &$filterDefinition) {
    if (empty($filterDefinition['pagedesigner_trait_type'])) {
      $filterDefinition['pagedesigner_trait_type'] = 'select';
    }
  }

}
