<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

/**
 * Process entities of type "taxonomyIndex".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_taxonomy_index",
 *   name = @Translation("TaxonomyIndex filter"),
 *   types = {
 *     "taxonomy_index_tid",
 *   },
 * )
 */
class TaxonomyIndex extends EntityFilter {

  /**
   * {@inheritDoc}
   */
  public function adjustFilter(array &$filterDefinition) {
    if (empty($filterDefinition['pagedesigner_trait_type'])) {
      $filterDefinition['pagedesigner_trait_type'] = 'multiplecheckbox';
    }
    if (empty($filterDefinition['entity_type'])) {
      $filterDefinition['entity_type'] = 'taxonomy_term';
    }
  }

}
