<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pagedesigner_block_adaptable\Plugin\FilterPluginBase;

/**
 * Process entities of type "datetime".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_datetime",
 *   name = @Translation("DateTime filter"),
 *   types = {
 *     "datetime",
 *   },
 * )
 */
class DateTime extends FilterPluginBase {
  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function view(array &$filterDefinition, $value) {
    return ['value' => $value];
  }

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition): array {
    if (!empty(\Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node')[$filterDefinition['field']])) {
      $label = (string) \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node')[$filterDefinition['field']]->getLabel();
    }
    else {
      $label = $this->t('Set value');
    }
    return [
      'label' => $label,
      'type' => 'text',
    ];
  }

}
