<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

/**
 * Process entities of type "UID Filter".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_uid",
 *   name = @Translation("UID filter"),
 *   types = {
 *     "user_name"
 *   }
 * )
 */
class UidFilter extends EntityFilter {

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition) : array {
    $renderArray = parent::build($id, $filterDefinition);
    if (isset($renderArray['options'][0])) {
      unset($renderArray['options'][0]);
    }
    return $renderArray;
  }

  /**
   * {@inheritDoc}
   */
  public function adjustFilter(array &$filterDefinition) {
    if (empty($filterDefinition['pagedesigner_trait_type'])) {
      $filterDefinition['pagedesigner_trait_type'] = 'multiplecheckbox';
    }
    $filterDefinition['entity_type'] = 'user';
  }

}
