<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Process entities of type "numeric".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_numeric",
 *   name = @Translation("Numeric filter"),
 *   types = {
 *     "numeric",
 *   },
 * )
 */
class Numeric extends EntityFilter {
  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition) : array {
    $this->adjustFilter($filterDefinition);
    $renderArray = [
      'label' => $filterDefinition['field'],
      'type' => 'text',
    ];
    // Handle reference fields (@deprecated in project:3.0.0).
    if (substr($filterDefinition['field'], -10) == '_target_id' && substr($filterDefinition['field'], 0, 6) == 'field_') {
      $bundle = substr($filterDefinition['field'], 6, -10);
      $items = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'type' => $bundle,
      ]);
      $options = [];
      $values = [];
      foreach ($items as $item) {
        if ($item != NULL) {
          $options[$item->id()] = $item->label();
        }
      }
      $renderArray = [
        'description' => $this->t('Choose @label', ['@label' => $filterDefinition['expose']['label']])->__toString(),
        'label' => $filterDefinition['expose']['label'],
        'options' => $options,
        'type' => $filterDefinition['pagedesigner_trait_type'],
        'name' => $id,
        'value' => $values,
      ];
    }
    else {
      $renderArray = parent::build($id, $filterDefinition);
    }
    return $renderArray;
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$filterDefinition, $value) {
    if (is_array($value)) {
      return reset($value);
    }
    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function adjustFilter(array &$filterDefinition) {
    if (empty($filterDefinition['pagedesigner_trait_type'])) {
      $filterDefinition['pagedesigner_trait_type'] = 'select';
    }
    if (empty($filterDefinition['entity_type'])) {
      $filterDefinition['entity_type'] = 'node';
    }
  }

}
