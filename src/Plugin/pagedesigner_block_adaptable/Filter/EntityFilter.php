<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\pagedesigner_block_adaptable\Plugin\FilterPluginBase;
use Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\FilterInterface;
use Drupal\pagedesigner_block_adaptable\Plugin\views\filter\EntityFilterBase;

/**
 * Process entities of type pba_entity_filter.
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_entity",
 *   name = @Translation("Entity filter plugin"),
 *   types = {
 *     "pba_entity_filter",
 *   }
 * )
 */
class EntityFilter extends FilterPluginBase {

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition) : array {
    $this->adjustFilter($filterDefinition);
    return self::getRenderArray($id, $filterDefinition);
  }

  /**
   * Returns a valid render array for filters for a content type.
   *
   * @param array $filterDefinition
   *   The filter definition.
   *
   * @return array
   *   The render array, fallback to text field.
   */
  public static function getRenderArray(string $id, array &$filterDefinition) {
    $renderArray = [
      'label' => $filterDefinition['field'],
      'type' => 'text',
    ];
    $entityType = (!empty($filterDefinition['entity_type'])) ? $filterDefinition['entity_type'] : NULL;
    if (empty($entityType)) {
      throw new \InvalidArgumentException("The entity_type key has to be set on the filter.");
    }
    $type = \Drupal::entityTypeManager()->getDefinition($entityType);
    if ($type instanceof ContentEntityType) {
      $options = [];
      $values = [];
      $bundles = EntityFilterBase::getBundles($filterDefinition);

      // Add sku on commerce variations.
      if ($entityType == "commerce_product_variation") {
        $data = EntityFilterBase::getData($type, $bundles, ['sku']);
        foreach ($data as $key => $record) {
          $options[$key] = $record->title . ' - ' . $record->sku;
        }
      }
      // Handle default cases (node, media, product, taxonomy terms etc.).
      else {
        $data = EntityFilterBase::getData($type, $bundles);
        $labelField = ($type->getKey('label')) ?: 'name';
        foreach ($data as $key => $record) {
          $options[$key] = $record->{$labelField};
        }
      }
      $values = array_keys($data);
      $renderArray = [
        'description' => t('Choose @label', ['@label' => $filterDefinition['expose']['label']])->__toString(),
        'label' => $filterDefinition['expose']['label'],
        'options' => $options,
        'type' => $filterDefinition['pagedesigner_trait_type'],
        'name' => $id,
        'value' => $values,
      ];
      if ($filterDefinition['pagedesigner_trait_type'] == 'autocomplete') {
        $renderArray['additional'] = [
          'autocomplete' => [
            'entity_type' => $type->id(),
            'bundles' => $bundles,
          ],
          'multiple' => $filterDefinition['pagedesigner_multiselect'],
        ];
      }
    }
    return $renderArray;
  }

  /**
   * {@inheritDoc}
   */
  public function patchPrepare(array &$filterDefinition, $value) {
    $this->adjustFilter($filterDefinition);
    return $this->filterValidEntries($filterDefinition, $value);
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$filterDefinition, $value) {
    $this->adjustFilter($filterDefinition);
    return $this->filterValidEntries($filterDefinition, $value);
  }

  /**
   * Filter out invalid entries.
   *
   * @param array $filterDefinition
   *   The filter definition.
   * @param mixed $value
   *   The value.
   *
   * @return mixed
   *   The filtered value.
   */
  protected function filterValidEntries(array &$filterDefinition, $value) {
    if ($filterDefinition['pagedesigner_trait_type'] == 'autocomplete') {
      if (is_array($value)) {
        $temp = [];
        foreach ($value as $entry) {
          if (is_numeric($entry)) {
            $temp[$entry] = $entry;
          }
          elseif (isset($entry['id'])) {
            $temp[$entry['id']] = $entry['id'];
          }
        }
        $value = $temp;
      }
      elseif (!empty($value['id'])) {
        $value = [$value['id'] => $value['id']];
      }
      elseif (is_numeric(reset($value))) {
        $value = [reset($value) => reset($value)];
      }
      else {
        $value = [];
      }
    }
    if ($filterDefinition['pagedesigner_trait_type'] == 'select') {
      if (is_array($value)) {
        $temp = [];
        foreach ($value as $entry) {
          $temp[$entry] = $entry;
        }
        $value = $temp;
      }
      elseif (is_numeric($value)) {
        $value = [$value => $value];
      }
      else {
        $value = [];
      }
    }
    $entityType = (!empty($filterDefinition['entity_type'])) ? $filterDefinition['entity_type'] : NULL;
    $type = \Drupal::entityTypeManager()->getDefinition($entityType);
    $bundles = EntityFilterBase::getBundles($filterDefinition);
    $validEntries = array_keys(EntityFilterBase::getData($type, $bundles));
    $hits = array_intersect(array_keys($value), $validEntries);
    $result = array_combine($hits, $hits);
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function serialize(array &$filterDefinition = NULL, $value = NULL) : array {
    $this->adjustFilter($filterDefinition);
    $values = [];
    if (empty($value)) {
      return $values;
    }
    if (is_scalar($value)) {
      $value = [$value => $value];
    }
    $autocomplete = FALSE;
    $storageManager = NULL;
    if (!empty($filterDefinition['pagedesigner_trait_type']) && $filterDefinition['pagedesigner_trait_type'] == 'autocomplete') {
      $autocomplete = TRUE;
      $entityType = (!empty($filterDefinition['entity_type'])) ? $filterDefinition['entity_type'] : NULL;
      $storageManager = \Drupal::entityTypeManager()->getStorage($entityType);
    }
    foreach ($value as $key => $item) {
      if ($item) {
        if ($autocomplete) {
          $entity = $storageManager->load($key);
          $values[] = [
            'label' => $entity->label(),
            'value' => $entity->label() . ' (' . $key . ')',
            'id' => $key,
          ];
        }
        else {
          $values[$key] = $key;
        }
      }
      else {
        unset($values[$key]);
      }
    }
    return $values;
  }

}
