<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pagedesigner_block_adaptable\Plugin\FilterPluginBase;

/**
 * Process entities of type "string".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_string",
 *   name = @Translation("String filter"),
 *   types = {
 *     "string",
 *   },
 * )
 */
class StringFilter extends FilterPluginBase {
  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition): array {
    if (!empty(\Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node')[$filterDefinition['field']])) {
      $label = (string) \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node')[$filterDefinition['field']]->getLabel();
    }
    else {
      $label = $this->t('Set value');
    }
    return [
      'label' => $label,
      'type' => 'text',
    ];
  }

}
