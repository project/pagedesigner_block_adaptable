<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pagedesigner_block_adaptable\Plugin\FilterPluginBase;

/**
 * Process entities of type "boolean".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_boolean",
 *   name = @Translation("Boolean filter"),
 *   types = {
 *     "boolean",
 *   },
 * )
 */
class Boolean extends FilterPluginBase {
  use StringTranslationTrait;

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition) : array {
    $storage_definitions = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions('node');

    if (!empty($storage_definitions[$filterDefinition['field']])) {
      $label = (string) $storage_definitions[$filterDefinition['field']]->getLabel();
    }
    else {
      if (substr($filterDefinition['field'], -6, 6) == '_value') {
        $field_name = substr($filterDefinition['field'], 0, strlen($filterDefinition['field']) - 6);
      }
      if (!empty($field_name)) {
        $label = (string) $storage_definitions[$field_name]->getLabel();
      }
      else {
        $label = $filterDefinition['field'];
      }
    }
    return [
      'description' => 'Select an option',
      'label' => $label,
      'options' => [
        '1' => $this->t('Yes'),
        '0' => $this->t('No'),
      ],
      'type' => 'select',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$filterDefinition, $value) {
    if (is_array($value)) {
      $value = reset($value);
    }
    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function patchPrepare(array &$filterDefinition, $value) {
    if (is_array($value)) {
      $value = reset($value);
    }
    return $value;
  }

}
