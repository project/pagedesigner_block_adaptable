<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable\Filter;

use Drupal\pagedesigner_block_adaptable\Plugin\FilterPluginBase;

/**
 * Process entities of type "bundle".
 *
 * @PagedesignerFilter(
 *   id = "pagedesigner_filter_bundle",
 *   name = @Translation("Bundle filter"),
 *   types = {
 *     "bundle",
 *   },
 * )
 */
class Bundle extends FilterPluginBase {

  /**
   * {@inheritDoc}
   */
  public function build(string $id, array &$filterDefinition): array {
    // Workaround for the content type, because there
    // can not be a key named 'type' in the definition.
    if ($filterDefinition['field'] === 'type') {
      $options = [];
      $values = [];
      foreach ($filterDefinition['value'] as $key => $option) {
        if (\Drupal::entityTypeManager()->getStorage('node_type')->load($option) != NULL) {
          $options[$key] = \Drupal::entityTypeManager()->getStorage('node_type')->load($option)->label();
          $values[$key] = TRUE;
        }
      }
      return [
        'description' => 'Choose type',
        'label' => 'Type',
        'options' => $options,
        'type' => 'multiplecheckbox',
        'name' => $id,
        'value' => $values,
      ];
    }
    else {
      $values = [];
      return [
        'description' => 'Choose options',
        'label' => $filterDefinition['field'],
        'options' => $filterDefinition['value'],
        'type' => 'multiplecheckbox',
        'name' => $id,
        'value' => $values,
      ];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function view(array &$filterDefinition, $value) {
    $result = [];
    foreach ($value as $filter_key => $item) {
      if ($item) {
        $result[$filter_key] = $filter_key;
      }
      else {
        unset($result[$filter_key]);
      }
    }
    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public function patchPrepare(array &$filterDefinition, $value) {
    $result = [];
    foreach ($value as $filter_key => $item) {
      if ($item) {
        $result[$filter_key] = $filter_key;
      }
      else {
        unset($result[$filter_key]);
      }
    }
    return $result;
  }

}
