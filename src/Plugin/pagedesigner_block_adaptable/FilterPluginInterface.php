<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner_block_adaptable;

/**
 * Provides the interface for defining pagedesigner block filters.
 */
interface FilterPluginInterface {

  /**
   * Returns a render array used for the filter in the view.
   *
   * @param string $id
   *   The view filter id.
   * @param array $filterDefinition
   *   The filter definition.
   *
   * @return array
   *   The render array.
   */
  public function build(string $id, array &$filterDefinition) : array;

  /**
   * Prepare the filter value for rendering.
   *
   * @param array $filterDefinition
   *   The filter definition.
   * @param mixed $value
   *   The value of the filter.
   *
   * @return mixed
   *   The resulting data to pass to the view.
   */
  public function view(array &$filterDefinition, $value);

  /**
   * Prepare the filter value for saving.
   *
   * @param array $filterDefinition
   *   The filter definition.
   * @param mixed $value
   *   The value of the filter.
   *
   * @return mixed
   *   The resulting data to save.
   */
  public function patchPrepare(array &$filterDefinition, $value);

  /**
   * Serializes the filter value.
   *
   * @param array $filterDefinition
   *   The value to serialize.
   * @param mixed $value
   *   The filter definition.
   *
   * @return array
   *   The serialized value.
   */
  public function serialize(array &$filterDefinition = NULL, $value = NULL) : array;

  /**
   * Adjust filter definition.
   *
   * @param array $filterDefinition
   *   The filter definition.
   */
  public function adjustFilter(array &$filterDefinition);

}
