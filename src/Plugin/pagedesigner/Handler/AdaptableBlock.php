<?php

namespace Drupal\pagedesigner_block_adaptable\Plugin\pagedesigner\Handler;

use Drupal\block\Entity\Block;
use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pagedesigner\ElementViewBuilder;
use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner\Plugin\HandlerPluginBase;
use Drupal\pagedesigner\Plugin\pagedesigner\HandlerPluginInterface;
use Drupal\pagedesigner_block_adaptable\AdaptableBlockViewBuilder;
use Drupal\ui_patterns\Definition\PatternDefinition;
use Drupal\ui_patterns\Definition\PatternDefinitionField;
use Drupal\views\Views;

/**
 * Process entities of type "block".
 *
 * Adds the ability to adapt the underlying view.
 *
 * @PagedesignerHandler(
 *   id = "adaptable_block",
 *   name = @Translation("Adaptable block handler"),
 *   types = {
 *      "block"
 *   },
 *   weight = 1000,
 * )
 */
class AdaptableBlock extends HandlerPluginBase implements HandlerPluginInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function collectAttachments(array &$attachments) {
  }

  /**
   * {@inheritDoc}
   */
  public function collectPatterns(array &$patterns, bool $render_markup = FALSE) {
    if ($render_markup) {
      foreach ($patterns as $pattern) {
        if (isset($pattern->getAdditional()['block'])) {
          $block = Block::load($pattern->getAdditional()['block']);
          $this->augmentDefinition($pattern, $block);
        }
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function adaptPatterns(array &$patterns) {
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(PatternDefinitionField &$field, array &$fieldArray) {
  }

  /**
   * Add the filters to the pattern definition.
   *
   * @param \Drupal\ui_patterns\Definition\PatternDefinition $pattern
   *   The pattern definition.
   * @param \Drupal\block\Entity\Block $block
   *   The block being processed.
   */
  protected function augmentDefinition(PatternDefinition &$pattern, Block &$block) {
    $pluginId = $block->getPluginId();
    if (strpos($pluginId, 'adaptable_views_block') !== 0) {
      return;
    }
    $viewInfo = explode('-', explode(':', $pluginId)[1]);
    $view = Views::getView($viewInfo[0]);
    if ($view == NULL) {
      return;
    }
    $view->setDisplay($viewInfo[1]);
    $display = $view->getDisplay();
    if ($display == NULL) {
      return;
    }
    $fields = $pattern->getFields();
    $filters = $view->getDisplay()->getOption('filters');
    $filterManager = \Drupal::service('plugin.manager.pagedesigner_block_adaptable_filter');

    // Expose the filters from the block so they can be
    // configurable in the pagedesigner for the editor.
    foreach ($filters as $id => $filter) {
      $filterPlugin = $filterManager->getInstance(['type' => $filter['plugin_id']])[0];
      if ($filter['plugin_id'] == 'bundle') {
        if ($filter['field'] === 'type') {
          $filter['bundle_filter'] = $filter;
        }
      }
      if ($filter['plugin_id'] == 'numeric') {
        $filter['filters'] = $filters;
      }
      if (empty($filter['exposed']) && strpos($filter['plugin_id'], 'pba_') === FALSE) {
        continue;
      }

      // Create multiple choice for the bundle plugin type.
      // Workaround for the content type, because there
      // can not be a key named 'type' in the definition.
      if ($filter['field'] === 'type') {
        $fields['content_type'] = $filterPlugin->build($id, $filter);
      }
      else {
        $fields[$id] = $filterPlugin->build($id, $filter);
      }
    }
    $pager = $view->getDisplay()->getOption('pager');
    if ($pager['type'] != 'none') {
      $fields['pager_items_per_page'] = [
        'description' => 'Select the number of items per page.',
        'label' => $this->t('Items per page') . ' (' . $this->t('Default: @value', ['@value' => $pager['options']['items_per_page']]) . ')',
        'type' => 'text',
        'name' => 'pager_items_per_page',
        'value' => '',
      ];
    }
    $fields['pager_offset'] = [
      'description' => 'Select the offset of items.',
      'label' => $this->t('Offset') . ' (' . $this->t('Default: @value', ['@value' => $pager['options']['offset']]) . ')',
      'type' => 'text',
      'name' => 'pager_offset',
      'value' => '',
    ];
    $pattern->setFields($fields);
  }

  /**
   * {@inheritdoc}
   */
  public function get(Element $entity, string &$result = '') {
  }

  /**
   * {@inheritdoc}
   */
  public function getContent(Element $entity, array &$list = [], $published = TRUE) {
  }

  /**
   * {@inheritDoc}
   */
  public function serialize(Element $entity, &$result = []) {
    $block = $entity->field_block->entity;
    if ($block != NULL && $entity->hasField('field_block_settings') && !$entity->field_block_settings->isEmpty()) {
      $fields = [];
      $viewInfo = explode('-', explode(':', $block->get('plugin'))[1]);
      $view = Views::getView($viewInfo[0]);
      // Check if there is a view for the block.
      if ($view == NULL) {
        return;
      }
      $view->setDisplay($viewInfo[1]);
      $display = $view->getDisplay();
      if ($display == NULL) {
        return;
      }
      // Take the filter data and apply it to the field of the entity.
      $filterDefinitions = $display->getOption('filters');
      $settings = Json::decode($entity->field_block_settings->value);
      $filterManager = \Drupal::service('plugin.manager.pagedesigner_block_adaptable_filter');
      if (!empty($settings['filters'])) {
        foreach ($settings['filters'] as $key => $item) {
          $filterDefinition = !empty($filterDefinitions[$key]) ? $filterDefinitions[$key] : NULL;
          if ($filterDefinition == NULL) {
            continue;
          }
          $filter = $filterManager->getInstance(['type' => $item['type']])[0];
          $fields[$key] = $filter->serialize($filterDefinition, $item['value']);
          if (!empty($filterDefinition['pagedesigner_trait_type']) && $filterDefinition['pagedesigner_trait_type'] == 'select') {
            $fields[$key] = [0 => (string) reset($fields[$key])];
          }
        }
      }
      if (!empty($settings['pager'])) {
        foreach ($settings['pager'] as $key => $item) {
          if (is_scalar($item)) {
            $fields['pager_' . $key] = $item;
          }
          else {
            $fields['pager_' . $key] = $item['value'];
          }
        }
      }
      $result = [
        'fields' => $fields,
      ] + $result;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function describe(Element $entity, array &$result = []) {
  }

  /**
   * {@inheritDoc}
   */
  public function generate($definition, array $data, Element &$entity = NULL) {
    $this->patch($entity, $data);
  }

  /**
   * {@inheritDoc}
   */
  public function patch(Element $entity, array $data) {
    $block = $entity->field_block->entity;
    if ($block != NULL && !empty($data['fields'])) {
      $view_filters = [];
      $viewInfo = explode('-', explode(':', $block->get('plugin'))[1]);
      $view = Views::getView($viewInfo[0]);
      // Check if there is a view for the block.
      if ($view == NULL) {
        return;
      }
      $view->setDisplay($viewInfo[1]);
      $display = $view->getDisplay();
      if ($display == NULL) {
        return;
      }
      // Take the filter data and apply it to the field of the entity.
      $filters = $display->getOption('filters');
      $filterManager = \Drupal::service('plugin.manager.pagedesigner_block_adaptable_filter');

      foreach ($data['fields'] as $key => $value) {
        if (isset($filters[$key])) {
          $handler = $filterManager->getInstance(['type' => $filters[$key]['plugin_id']])[0];
          $view_filters[$key]['value'] = $handler->patchPrepare($filters[$key], $value);
          $view_filters[$key]['type'] = $filters[$key]['plugin_id'];
        }
        elseif ($key == 'content_type') {
          foreach ($value as $filter_key => $item) {
            if ($item) {
              $view_filters[$key]['value'][$filter_key] = TRUE;
            }
            else {
              $view_filters[$key]['value'][$filter_key] = FALSE;
              $view_filters[$key]['value']['all'] = FALSE;
            }
          }
        }
      }

      $pagerSettings = [];
      if (isset($data['fields']['pager_items_per_page'])) {
        $pagerSettings['items_per_page'] = $data['fields']['pager_items_per_page'];
      }
      if (isset($data['fields']['pager_offset'])) {
        $pagerSettings['offset'] = $data['fields']['pager_offset'];
      }
      $entity->field_block_settings->value = Json::encode(
        ['filters' => $view_filters, 'pager' => $pagerSettings]
      );
      $entity->save();
      \Drupal::service('cache_tags.invalidator')->invalidateTags($view->storage->getCacheTagsToInvalidate());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function copy(Element $entity, Element $container = NULL, Element &$clone = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function delete(Element $entity, bool $remove = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public function restore(Element $entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function view(Element $entity, string $view_mode, array &$build = []) {

  }

  /**
   * {@inheritdoc}
   */
  public function build(Element $entity, string $view_mode, array &$build = []) {
    if ($entity->field_block->entity == NULL) {
      return;
    }
    if ($entity->hasField('field_block_settings') && !$entity->field_block_settings->isEmpty() && strpos($entity->field_block->entity->getPluginId(), 'adaptable_views_block') === 0) {
      $sub_build = AdaptableBlockViewBuilder::lazyBuilderWithElement($entity->field_block->target_id, $entity);
      if ($view_mode == ElementViewBuilder::RENDER_MODE_EDIT) {
        $build['pagedesigner_block']['#context']['markup'] = $sub_build;
      }
      else {
        unset($build['pagedesigner_block']['#lazy_builder']);
        $build['pagedesigner_block'][0] = $sub_build;
        $build['pagedesigner_block']['#cache']['keys'] = array_merge(
          $build['pagedesigner_block']['#cache']['keys'],
          ['pagedesigner_element', $entity->id(), $view_mode]
        );
        $this->renderer->addCacheableDependency($build['pagedesigner_block'], $entity);
        $this->viewBuilder->adjustCacheTags($build['pagedesigner_block'], $view_mode);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function publish(Element $entity) {
  }

  /**
   * {@inheritdoc}
   */
  public function unpublish(Element $entity) {
  }

}
