<?php

namespace Drupal\pagedesigner_block_adaptable;

use Drupal\block\BlockViewBuilder;
use Drupal\block\Entity\Block;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\pagedesigner\Entity\Element;
use Drupal\pagedesigner_block_adaptable\Plugin\Block\AdaptableViewsBlockInterface;

/**
 * Provides a Block view builder.
 */
class AdaptableBlockViewBuilder extends BlockViewBuilder {

  /**
   * Lazy builder callback.
   *
   * @param \Drupal\block\Entity\Block $entity
   *   The block to render.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\pagedesigner\Entity\Element $element
   *   The pagedesigner element.
   *
   * @return array
   *   The build array.
   */
  protected static function buildPreRenderableBlockWithElement(Block $entity, ModuleHandlerInterface $module_handler, Element $element) {
    $build = parent::buildPreRenderableBlock($entity, $module_handler);
    $build['#pagedesigner_element'] = $element;
    return $build;
  }

  /**
   * Lazy builder callback.
   *
   * @param int $block_id
   *   The block to render.
   * @param \Drupal\pagedesigner\Entity\Element $element
   *   The pagedesigner element.
   *
   * @return array
   *   The build array.
   */
  public static function lazyBuilderWithElement($block_id, Element $element) {
    return static::buildPreRenderableBlockWithElement(Block::load($block_id), \Drupal::service('module_handler'), $element);
  }

  /**
   * {@inheritdoc}
   */
  public static function preRender($build) {
    $plugin = $build['#block']->getPlugin();
    if ($plugin instanceof AdaptableViewsBlockInterface) {
      $plugin->setPagedesignerElement($build['#pagedesigner_element']);
    }
    return parent::preRender($build);
  }

}
